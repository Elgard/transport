﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transport.Code;
using Transport.Code.TransportTask;
using Transport.MapLoad;
using Transport.Windows;

namespace Transport
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = Center.Instance;
            if (string.IsNullOrEmpty(Center.Instance.Address))
            {
                Center.Instance.Address = "Миколаїв, Миколаївська область";
            }
            foreach (var value in Center.Instance.Clients.Values)
            {
                clientsListBox.Items.Add(value);
            }
            foreach (var value in Center.Instance.Transports)
            {
                transportListBox.Items.Add(value);
            }
        }

        public void MapRefresh()
        {
            var marker = new GoogleMapMarker
            {
                Color = GoogleMapColor.red,
                Size = GoogleMapMarkerSize.Mid,
                Label = "Client",
                Locations = new GoogleMapLocation[Center.Instance.Clients.Count]
            };
            int i = 0;
            foreach (var client in Center.Instance.Clients.Values)
            {
                marker.Locations[i++] = client.Location;
            }
            var mapLoder = new MapLoader(
                new GoogleMapLocation
                {
                    Address = Center.Instance.Address

                },
                new[]{
                    new GoogleMapMarker
                    {
                        Color = GoogleMapColor.black,
                        Locations = new []
                        {
                            new GoogleMapLocation {Address = Center.Instance.Address }
                        },
                        Label = "Center",
                        Size =  GoogleMapMarkerSize.Mid
                    },
                    marker

                },
                Center.Instance.Route.Select((locations, index) => new GoogleMapPath
                {
                    Locations = locations,
                    Color = (GoogleMapColor)index
                }).ToArray());
            mapLoder.OnMapLoad += image =>
            {
                MapImage.Width = image.Width;
                MapImage.Height = image.Height;
                MapImage.Source = image;
            };
            mapLoder.Refresh();
        }

        private void AddClient_OnClick(object sender, RoutedEventArgs e)
        {
            var clientWindow = new ClientWindow(new Client());
            clientWindow.OnSaveClose += (o) =>
            {
                var window = ((ClientWindow)o);
                if (!window.SaveState) return;
                if (Center.Instance.Clients.Keys.Count > 0)
                    Center.Instance.Clients[Center.Instance.Clients.Keys.Max() + 1] = window.CurrentClient;
                else
                    Center.Instance.Clients[0] = window.CurrentClient;
                clientsListBox.Items.Add(window.CurrentClient);
            };
            clientWindow.ShowDialog();
        }
        private void AddClientAgent_OnClick(object sender, RoutedEventArgs e)
        {
            var clientWindow = new ClientAgentWindow(new Client());
            clientWindow.OnSaveClose += (o) =>
            {
                var window = ((ClientAgentWindow)o);
                if (!window.SaveState) return;
                var cla = new ClientAgent(window.CurrentClient);
                Center.Instance.ClientAgents.Add(cla);
                Center.Instance.ClientAgents.Add(new ClientAgent(cla.TargrtClient));
                Center.Instance.RangeTransportAgents(new WayPoint
                {
                    Location = cla.Location,
                    ClientAgent = cla
                });
                clientAgentList.Items.Refresh();
            };
            clientWindow.ShowDialog();
        }
        private void RemoveClient_OnClick(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < clientsListBox.SelectedItems.Count; i++)
            {
                var item = clientsListBox.SelectedItems[i];
                var client = (Client) item;
                if (Center.Instance.Clients.ContainsValue(client))
                {
                    Center.Instance.Clients.Remove(Center.Instance.Clients.First(pair => client.Equals(pair.Value)).Key);
                    clientsListBox.Items.Remove(item);
                }
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            MapRefresh();
        }

        private void AddTransport_OnClick(object sender, RoutedEventArgs e)
        {
            var transportWindow = new TransportWindow();
            transportWindow.Closing += (o, args) =>
            {
                var window = ((TransportWindow)o);
                if (!window.SaveState) return;
                Center.Instance.Transports.Add(window.CurrentTransport);
                transportListBox.Items.Add(window.CurrentTransport);
            };
            transportWindow.ShowDialog();
        }

        private void RemoveTransport_OnClick(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < transportListBox.SelectedItems.Count; i++)
            {
                var item = transportListBox.SelectedItems[i];
                var transport = (Code.Transport) item;
                if (Center.Instance.Transports.Contains(transport))
                {
                    Center.Instance.Transports.Remove(transport);
                    transportListBox.Items.Remove(item);
                }
            }
        }

        private void TimeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            foreach (var agent in Center.Instance.TransportAgents)
            {
                agent.SetTime((float)e.NewValue);
            }
            transportAgentList.Items.Refresh();
            clientAgentList.Items.Refresh();
        }

        private void Run_Click(object sender, RoutedEventArgs e)
        {
            Center.Instance.FindPath();
            transportAgentList.ItemsSource = Center.Instance.TransportAgents;
            clientAgentList.ItemsSource = Center.Instance.ClientAgents;
        }

        private void DistanceMatrix_Click(object sender, RoutedEventArgs e)
        {
            (new DistanceWindow()).ShowDialog();
        }

        private void wayListbtn_Click(object sender, RoutedEventArgs e)
        {
            new WayWindow().ShowDialog();
        }

        private void transportAgentList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = transportAgentList.SelectedItems[0];
                new TransportAgentWindow((TransportAgent) item).ShowDialog();
            }
            catch
            {
                //ignore
            }
            

        }

        
    }
}
