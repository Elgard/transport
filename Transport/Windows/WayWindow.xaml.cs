﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transport.Code;

namespace Transport.Windows
{
    /// <summary>
    /// Логика взаимодействия для WayWindow.xaml
    /// </summary>
    public partial class WayWindow : Window
    {
        public WayWindow()
        {
            InitializeComponent();
            listBox.ItemsSource = Center.Instance.Way.Select(location => location.Address);
        }
    }
}
