﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transport.Code.TransportTask;

namespace Transport.Windows
{
    /// <summary>
    /// Логика взаимодействия для TransportAgentWindow.xaml
    /// </summary>
    public partial class TransportAgentWindow : Window
    {
        public TransportAgentWindow(TransportAgent agent)
        {
            InitializeComponent();
            labelName.Content = agent.Name;
            labelSpeed.Content = agent.Speed;
            labelMoney.Content = agent.Money- agent.MoneyLoose;
            labelDistance.Content = agent.Distance;
            listWay.ItemsSource =
                agent.TimePoints.Select(pair => $"Location {pair.Value.Location.Address} in time {pair.Key}");
        }
    }
}
