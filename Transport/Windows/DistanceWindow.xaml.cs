﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transport.Code;

namespace Transport.Windows
{
    /// <summary>
    /// Логика взаимодействия для DistanceWindow.xaml
    /// </summary>
    public partial class DistanceWindow : Window
    {
        public DistanceWindow()
        {
            InitializeComponent();
            dataGrid.ShowGridLines = true;
            for (int i = 0; i < Center.Instance.DistanceMatrix.GetLength(0); i++)
            {
                dataGrid.ColumnDefinitions.Add(new ColumnDefinition());
                dataGrid.RowDefinitions.Add(new RowDefinition());
            }
            
            for (int i = 0; i < Center.Instance.DistanceMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < Center.Instance.DistanceMatrix.GetLength(1); j++)
                {
                    var label = new Label();
                    dataGrid.Children.Add(label);
                    label.Content = Center.Instance.DistanceMatrix[i, j];
                    label.SetValue(Grid.ColumnProperty, i);
                    label.SetValue(Grid.RowProperty, j);
                }

            }
        }
    }
}
