﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transport.Code;

namespace Transport.Windows
{
    /// <summary>
    /// Логика взаимодействия для ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window
    {
        public Client CurrentClient { get; }
        public bool SaveState { get; private set; }
        public ClientWindow(Client client)
        {
            InitializeComponent();
            CurrentClient = client;
            DataContext = client;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            OnClose();
            SaveState = true;
            if (OnSaveClose != null) OnSaveClose(this);
            Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnClose()
        {
            CurrentClient.Location.Address = addres.Text;
            CurrentClient.Mass = int.Parse(mass.Text);
            CurrentClient.Price = float.Parse(price.Text);
            //if (CurrentClient.IsTransportOnly)
            //    CurrentClient.TargrtClient = (Client) targetClient.SelectionBoxItem;
        }

        public event Action<Object> OnSaveClose;
    }
}
