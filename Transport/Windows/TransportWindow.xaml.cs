﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Transport.Windows
{
    /// <summary>
    /// Логика взаимодействия для TransportWindow.xaml
    /// </summary>
    public partial class TransportWindow : Window
    {
        public Code.Transport CurrentTransport { get; private set; }
        public bool SaveState { get; private set; }
        public TransportWindow()
        {
            InitializeComponent();
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            SaveState = true;
            CurrentTransport = new Code.Transport
            {
                Name = name.Text,
                Capacity = int.Parse(capacity.Text),
                Speed = float.Parse(speed.Text),
                MonetaryLossPerMeter = float.Parse(monetary.Text)
            };
            Close();
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
