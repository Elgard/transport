﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Transport.GoogleAPI
{
    public class GeocodedWaypoint
    {

        [JsonProperty("geocoder_status")]
        public string GeocoderStatus { get; set; }

        [JsonProperty("place_id")]
        public string PlaceId { get; set; }

        [JsonProperty("types")]
        public IList<string> Types { get; set; }
    }
}
