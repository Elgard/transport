﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Transport.MapLoad;

namespace Transport.GoogleAPI
{
    public class OverviewPolyline
    {

        [JsonProperty("points")]
        public string Points { get; set; }

        public static implicit operator GoogleMapLocation[] (OverviewPolyline polyline)
        {
            var len = polyline.Points.Length;
            var index = 0;
            var decoded = new List<GoogleMapLocation>();
            var lat = 0;
            var lng = 0;

            while (index < len)
            {
                int b;
                var shift = 0;
                var result = 0;
                do
                {
                    b = polyline.Points[index++] - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do
                {
                    b = polyline.Points[index++] - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                decoded.Add(new GoogleMapLocation
                {
                    Latitude = ((float)lat) / 100000f,
                    Longitude = ((float)lng) / 100000f
                });
            }

            return decoded.ToArray();
        }
    }
}
