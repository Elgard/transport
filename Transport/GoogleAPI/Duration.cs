﻿using Newtonsoft.Json;

namespace Transport.GoogleAPI
{
    public class Duration
    {

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
