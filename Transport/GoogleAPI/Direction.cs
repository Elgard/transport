﻿using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace Transport.GoogleAPI
{
    public class Direction
    {
        private const string UrlTemplate =
            @"https://maps.googleapis.com/maps/api/directions/json?origin={0}&destination={1}&key=AIzaSyAnIk7cZ9_NTH8T_vITVWAt0CSyMbrmznw";
        public Direction() { }
        public Direction(string origin, string destination)
        {
            Origin = origin;
            Destination = destination;
            using (var client = new WebClient())
            {
                var response = client.DownloadString(string.Format(UrlTemplate, origin, destination));
                var tmpObj = JsonConvert.DeserializeObject<Direction>(response);
                GeocodedWaypoints = tmpObj.GeocodedWaypoints;
                Routes = tmpObj.Routes;
                Status = tmpObj.Status;
            }
        }
        [JsonProperty("geocoded_waypoints")]
        public IList<GeocodedWaypoint> GeocodedWaypoints { get; set; }

        [JsonProperty("routes")]
        public IList<Route> Routes { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonIgnore]
        public string Origin { get; set; }
        [JsonIgnore]
        public string Destination { get; set; }
    }
}
