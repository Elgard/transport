﻿using Newtonsoft.Json;

namespace Transport.GoogleAPI
{
    public class Polyline
    {

        [JsonProperty("points")]
        public string Points { get; set; }
    }
}
