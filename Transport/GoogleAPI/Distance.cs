﻿using Newtonsoft.Json;

namespace Transport.GoogleAPI
{
    public class Distance
    {

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }
    }
}
