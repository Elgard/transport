﻿using Newtonsoft.Json;

namespace Transport.GoogleAPI
{
    public class Bounds
    {

        [JsonProperty("northeast")]
        public Northeast Northeast { get; set; }

        [JsonProperty("southwest")]
        public Southwest Southwest { get; set; }
    }
}
