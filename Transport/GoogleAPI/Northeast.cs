﻿using Newtonsoft.Json;

namespace Transport.GoogleAPI
{
    public class Northeast
    {

        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lng")]
        public double Lng { get; set; }
    }
}
