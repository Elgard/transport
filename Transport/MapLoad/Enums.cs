﻿namespace Transport.MapLoad
{
    public enum GoogleMapColor
    {
        black,
        brown,
        green,
        purple,
        yellow,
        blue,
        gray,
        orange,
        red,
        white
    }
    public enum MapType
    {
        RoadMap,
        Satellite,
        Terrain,
        Hybrid
    }
    public enum GoogleMapMarkerSize
    {
        Tiny,
        Small,
        Mid
    }

    public enum TransportType
    {
        Nun,
        Tram=1,
        TramMed=2,
        Trolley=3,
        TrolleyMed=4,
        Minibus=5,
        Excavator=6,
        Snowblower=7,
        GarbageCollector=8

    }

    public enum StatusBarAction
    {
        Show,
        Hide,
        Error,
        Restore,
        Close
    }
}
