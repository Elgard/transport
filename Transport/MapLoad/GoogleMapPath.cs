﻿using System;

namespace Transport.MapLoad
{
    [Serializable]
    public class GoogleMapPath
    {
        public int weight = 10;
        public GoogleMapColor Color = GoogleMapColor.green;
        public bool fill = false;
        public GoogleMapColor FillColor = GoogleMapColor.green;
        public GoogleMapLocation[] Locations;
    }
}
