﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Windows.Media.Imaging;

namespace Transport.MapLoad
{
    class MapLoader
    {
        public MapLoader(GoogleMapMarker[] markers, GoogleMapPath[] paths)
        {
            _autoLocateCenter = true;
            _markers = markers ?? new GoogleMapMarker[0];
            _paths = paths ?? new GoogleMapPath[0];
            if(_markers.Length==0 && _paths.Length==0)throw new Exception("Шта?");
        }
        public MapLoader(GoogleMapLocation centerLocation, GoogleMapMarker[] markers, GoogleMapPath[] paths)
        {
            _autoLocateCenter = false;
            _centerLocation = centerLocation;
            if(_centerLocation==null) throw new Exception("Шта?");
            _markers = markers ?? new GoogleMapMarker[0];
            _paths = paths ?? new GoogleMapPath[0];
            //var p = new List<GoogleMapLocation>(_paths[0].Locations);
            //var length = p.Count*0.7f;
            //var random = new Random();
            //for (int i = 0; i < length; i++)
            //{
            //    p.Remove(p[random.Next(p.Count)]);
            //}
            //_paths[0].Locations = p.ToArray();
        }



        private readonly bool _autoLocateCenter;
        private readonly GoogleMapLocation _centerLocation;
        private int zoom = 5;
        private MapType _mapType = MapType.RoadMap;
        private int size = 512;
        private bool doubleResolution = false;
        private readonly GoogleMapMarker[] _markers;
        private readonly GoogleMapPath[] _paths;


        public MapType MapType { get { return _mapType; } set { _mapType = value; } }

        public async void Refresh()
        {
            const string url = "http://maps.googleapis.com/maps/api/staticmap";
            var qs = "";
            if (!_autoLocateCenter)
            {
                if (_centerLocation.Address != "")
                    qs += "center=" + _centerLocation.Address;
                else
                {
                    qs += "center=" + $"{_centerLocation.Latitude},{_centerLocation.Longitude}";
                }

                //qs += "&zoom=" + zoom;
            }
            qs += "&size=" + string.Format("{0}x{0}", size);
            qs += "&scale=" + (doubleResolution ? "2" : "1");
            qs += "&maptype=" + _mapType.ToString().ToLower();

            qs += "&sensor=" + false;

            foreach (var i in _markers)
            {
                qs += "&markers=" + $"size:{i.Size.ToString().ToLower()}|color:{i.Color}|label:{i.Label}";
                foreach (var loc in i.Locations)
                {
                    if (loc.Address != "")
                        qs += "|" + loc.Address;
                    else
                        qs += "|" + $"{loc.Latitude},{loc.Longitude}";
                }
            }

            foreach (var i in _paths)
            {
                qs += "&path=" + $"weight:{i.weight}|color:{i.Color}";
                if (i.fill) qs += "|fillcolor:" + i.FillColor;
                foreach (var loc in i.Locations)
                {
                    if (!string.IsNullOrEmpty(loc.Address))
                        qs += "|" + loc.Address;
                    else
                        qs += "|" + $"{loc.Latitude.ToString(new CultureInfo("en-US"))},{loc.Longitude.ToString(new CultureInfo("en-US"))}";
                }
            }
            byte[] bytes;
            using (var client = new WebClient())
            {
                bytes = await client.DownloadDataTaskAsync(new Uri(url + "?" + qs));
                if (bytes==null || bytes.Length==0)throw new Exception("LoadException");
            }
            OnMapLoad?.Invoke(LoadImage(bytes));
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }
        public event Action<BitmapImage> OnMapLoad;
    }
}
