﻿using System;

namespace Transport.MapLoad
{
    [Serializable]
    public class GoogleMapLocation
    {
        public string Address;
        public float Latitude;
        public float Longitude;

        public float Distance(GoogleMapLocation to)
        {
            var rad = 6372795;


            var llat1 = Latitude;
            var llong1 = Longitude;


            var llat2 = to.Latitude;
            var llong2 = to.Longitude;


            var lat1 = llat1 * Math.PI / 180;
            var lat2 = llat2 * Math.PI / 180;
            var long1 = llong1 * Math.PI / 180;
            var long2 = llong2 * Math.PI / 180;


            var cl1 = Math.Cos(lat1);
            var cl2 = Math.Cos(lat2);
            var sl1 = Math.Sin(lat1);
            var sl2 = Math.Sin(lat2);
            var delta = long2 - long1;
            var cdelta = Math.Cos(delta);
            var sdelta = Math.Sin(delta);


            var y = Math.Sqrt(Math.Pow(cl2 * sdelta, 2) + Math.Pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
            var x = sl1 * sl2 + cl1 * cl2 * cdelta;
            var ad = Math.Atan2(y, x);
            return (float) ad * rad;
        }
    }
}
