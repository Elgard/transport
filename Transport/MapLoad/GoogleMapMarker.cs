﻿using System;

namespace Transport.MapLoad
{
    [Serializable]
    public class GoogleMapMarker
    {
        
        public GoogleMapMarkerSize Size;
        public GoogleMapColor Color;
        public string Label;
        public GoogleMapLocation[] Locations;

    }
}
