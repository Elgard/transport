﻿using System;
using Transport.MapLoad;

namespace Transport.Code
{
    [Serializable]
    public class Client
    {
        public Client()
        {
            Location = new GoogleMapLocation();
        }

        protected Client(Client client)
        {
            Location = client.Location;
            DistanceFromCenter = client.DistanceFromCenter;
            Price = client.Price;
            Mass = client.Mass;
            IsTransportOnly = client.IsTransportOnly;
            TargrtClient = client.TargrtClient;
        }
        public GoogleMapLocation Location { get; set; }
        public int DistanceFromCenter { get; set; }
        public float Price { get; set; }
        public float Mass { get; set; }
        public bool IsTransportOnly { get; set; }
        public Client TargrtClient { get; set; }
        public override bool Equals(object obj)
        {
            var client = obj as Client;
            if (client == null) return false;
            return Location == client.Location;
        }

        public override string ToString()
        {
            return $"Address: {Location.Address} Price: {Price} Mass: {Mass}";
        }
        public static explicit operator Client(string c)
        {
            var sp = c.Split('|');
            return new Client
            {
                Location = new GoogleMapLocation {Address = sp[0] } ,
                Price = float.Parse(sp[1]),
                Mass = int.Parse(sp[2])
            };
        }
    }
}
