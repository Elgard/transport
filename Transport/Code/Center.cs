﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using Newtonsoft.Json;
using Transport.Code.TransportTask;
using Transport.GoogleAPI;
using Transport.MapLoad;

namespace Transport.Code
{
    [Serializable]
    public class Center
    {
        private static Center _instance;
        public static Center Instance
        {
            get
            {
                if (_instance != null) return _instance;
                var path = Directory.GetCurrentDirectory() + @"/Center";
                if (File.Exists(path))
                {
                    _instance = JsonConvert.DeserializeObject<Center>(File.ReadAllText(path));
                }
                else
                {
                    return (_instance = new Center());
                }
                return _instance;
            }
        }


        public string Address { get; set; }
        public List<Transport> Transports { get; set; }
        public Dictionary<int, Client> Clients { get; set; }
        public float[,] DistanceMatrix { get; set; }
        public List<TransportAgent> TransportAgents { get; set; }
        public List<ClientAgent> ClientAgents { get; set; }
        public List<Direction> Directions { get; set; }
        public List<GoogleMapLocation[]> Route { get; set; }
        public GoogleMapLocation[] Way { get; set; }

        private List<GoogleMapLocation> _wayPoints;
        private List<Lines> _lineses;


        private Center()
        {
            var path = Directory.GetCurrentDirectory() + @"/Center";
            if (File.Exists(path)) return;
            Address = string.Empty;
            Transports = new List<Transport>();
            Clients = new Dictionary<int, Client>();
        }

        ~Center()
        {
            File.WriteAllText(Directory.GetCurrentDirectory() + @"/Center", JsonConvert.SerializeObject(this));
        }

        public void FindPath()
        {
            GenerateDistanceMatrix();
            TransportAgents = new List<TransportAgent>();
            ClientAgents = new List<ClientAgent>();

            var seving = new Seving(DistanceMatrix, _wayPoints.ToArray());
            Way = seving.GetWay();

            Route = new List<GoogleMapLocation[]>
            {
                _lineses.Find(line => line.Origin == Way[0] && line.Destination == Way[1]).Route.OverviewPolyline
            };
            for (int i = 0; i < Way.Length - 1; i++)
            {
                var rout = _lineses.Find(line => line.Origin == Way[i] && line.Destination == Way[i + 1]).Route;
                Route.Add(rout.OverviewPolyline);
            }
            Route.Add(_lineses.Find(line => line.Origin == Way[Way.Length - 1] && line.Destination == Way[0]).Route.OverviewPolyline);

            for (int i = 0; i < Route.Count; i++)
            {
                var p = new List<GoogleMapLocation>(Route[i]);
                var length = p.Count * 0.8f;
                var random = new Random();
                for (int j = 0; j < length; j++)
                {
                    p.Remove(p[random.Next(p.Count)]);
                }
                Route[i] = p.ToArray();
            }
            CreateAgents();
        }



        public void AddClient(Client client)
        {
            if (Clients.Count > 0)
                Clients.Add(Clients.Keys.Max() + 1, client);
            Clients.Add(0, client);
        }

        public void RemoveClient(Client client)
        {
            if (!Clients.ContainsValue(client)) return;
            Clients.Remove(Clients.First(c => c.Value == client).Key);
        }


        private void CreateAgents()
        {
            var transports = Transports.GetEnumerator();
            if (!transports.MoveNext()) return;
            var way = new List<WayPoint>
            {
                new WayPoint
                {
                    Distance = 0,
                    Location = _wayPoints.First()
                }
            };
            var transport = transports.Current;
            float waigth = transport.Capacity;
            var steps = new List<GoogleMapLocation>();

            foreach (var location in Way)
            {
                var targetClient = Clients.Values.Single(client => client.Location.Address == location.Address);
                var cla = new ClientAgent(targetClient);
                ClientAgents.Add(cla);
                waigth -= targetClient.Mass;
                if (waigth < 0)
                {
                    TransportAgents.Add(new TransportAgent(way, transports.Current, steps.ToArray()));
                    if (!transports.MoveNext()) return;
                    steps = new List<GoogleMapLocation>();
                    way = new List<WayPoint>
                    {
                        new WayPoint
                        {
                            Distance = 0,
                            Location = _wayPoints.First()
                        }
                    };
                    transport = transports.Current;
                    waigth = transport.Capacity - targetClient.Mass;
                }
                steps.AddRange((GoogleMapLocation[])
                    Directions.Find(
                        direction => 
                        (direction.Origin == way.Last().Location.Address||
                        direction.Destination == way.Last().Location.Address)&&
                        (direction.Origin == location.Address ||
                        direction.Destination == location.Address)
                        ).Routes[0].OverviewPolyline);
                way.Add(new WayPoint
                {
                    Location = location,
                    Distance = DistanceMatrix[_wayPoints.IndexOf(way.Last().Location), _wayPoints.IndexOf(location)],
                    ClientAgent = cla
                });
            }
            TransportAgents.Add(new TransportAgent(way, transports.Current, steps.ToArray()));
        }

        private void GenerateDistanceMatrix()
        {
            _lineses = new List<Lines>();
            _wayPoints = Clients.Select(c => c.Value.Location).ToList();
            _wayPoints.Insert(0, new GoogleMapLocation { Address = Address });
            int i = 0, j = 0;
            DistanceMatrix = new float[_wayPoints.Count, _wayPoints.Count];
            Directions = new List<Direction>(_wayPoints.Count * 2 - _wayPoints.Count);
            foreach (var origin in _wayPoints)
            {
                foreach (var destination in _wayPoints)
                {
                    if (origin == destination)
                    {
                        DistanceMatrix[i, j++] = 0;
                    }
                    else
                    {
                        var direction = new Direction(origin.Address, destination.Address);
                        Directions.Add(direction);
                        DistanceMatrix[i, j++] = direction.Routes[0].Legs[0].Distance.Value;
                        var way = ((GoogleMapLocation[]) direction.Routes[0].OverviewPolyline);
                        origin.Latitude = way[0].Latitude;
                        destination.Latitude = way.Last().Latitude;
                        origin.Longitude = way[0].Longitude;
                        destination.Longitude = way.Last().Longitude;
                        
                        _lineses.Add(new Lines
                        {
                            Origin = origin,
                            Destination = destination,
                            Route = direction.Routes[0]
                        });
                    }
                }
                j = 0;
                i++;
            }
        }

        public void RangeTransportAgents(WayPoint point)
        {
            var transports = new Dictionary<TransportAgent, Direction>();
            foreach (var agent in TransportAgents)
            {
                var direction =
                    new Direction(
                        $"{agent.Location.Latitude.ToString(new CultureInfo("en-US"))},{agent.Location.Longitude.ToString(new CultureInfo("en-US"))}",
                        point.Location.Address);
                transports[agent] = direction;
            }

            var orderedTransport =
                transports.OrderBy(pair => pair.Value.Routes[0].Legs[0].Distance.Value)
                    .Select(pair => pair.Key)
                    .ToList();
            foreach (var agent in orderedTransport)
            {
                if (point.ClientAgent.Mass + agent.CurrentCapacity > agent.Capacity) continue;
                var wayPoints =
                    agent.TimePoints.Values.Where(
                        wayPoint => wayPoint.ClientAgent != null && !wayPoint.ClientAgent.IsDone)
                        .Select(wayPoint => wayPoint.Location).ToList();
                wayPoints.Add(point.Location);
                wayPoints.Add(point.ClientAgent.TargrtClient.Location);
                agent.Location.Address =
                    $"{agent.Location.Latitude.ToString(new CultureInfo("en-US"))},{agent.Location.Longitude.ToString(new CultureInfo("en-US"))}";
                wayPoints.Insert(0, agent.Location);

                int i = 0, j = 0;
                var distanceMatrix = new float[wayPoints.Count, wayPoints.Count];
                var directions = new List<Direction>(wayPoints.Count*2 - wayPoints.Count);

                foreach (var origin in wayPoints)
                {
                    foreach (var destination in wayPoints)
                    {
                        if (origin == destination)
                        {
                            distanceMatrix[i, j++] = 0;
                        }
                        else
                        {
                            var direction = new Direction(origin.Address, destination.Address);
                            directions.Add(direction);
                            distanceMatrix[i, j++] = direction.Routes[0].Legs[0].Distance.Value;
                            var lway = ((GoogleMapLocation[]) direction.Routes[0].OverviewPolyline);
                            origin.Latitude = lway[0].Latitude;
                            destination.Latitude = lway.Last().Latitude;
                            origin.Longitude = lway[0].Longitude;
                        }
                    }
                    j = 0;
                    i++;
                }
                var sewing = new Seving(distanceMatrix, wayPoints.ToArray());
                var sewingWay = sewing.GetWay();
                var steps = new List<GoogleMapLocation>();
                var way = new List<WayPoint>
                {
                    new WayPoint
                    {
                        Distance = 0,
                        Location = agent.Location
                    }
                };

                

                foreach (var location in sewingWay)
                {
                    var cla = ClientAgents.Single(client => client.Location.Address == location.Address);
                    
                    steps.AddRange((GoogleMapLocation[])
                        directions.Find(
                            direction =>
                            (direction.Origin == way.Last().Location.Address ||
                            direction.Destination == way.Last().Location.Address) &&
                            (direction.Origin == location.Address ||
                            direction.Destination == location.Address)
                            ).Routes[0].OverviewPolyline);
                    way.Add(new WayPoint
                    {
                        Location = location,
                        Distance = distanceMatrix[wayPoints.IndexOf(way.Last().Location), wayPoints.IndexOf(location)],
                        ClientAgent = cla
                    });
                }


                agent.EditWay(way, steps.ToArray());
                return;
            }
            MessageBox.Show("Transport agent not found");
        }

        private class Lines
        {
            public GoogleMapLocation Origin { get; set; }
            public GoogleMapLocation Destination { get; set; }
            public Route Route { get; set; }
        }
    }
}
