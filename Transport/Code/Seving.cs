﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transport.MapLoad;

namespace Transport.Code
{
    class Seving
    {
        private readonly SavingTableElement[] _savingTable;
        private readonly int _wayLength;
        public Seving(float[,] data, GoogleMapLocation[] locations)
        {
            var length=0;

            _wayLength = locations.Length -1;
            for (var i = 0; i < data.GetLength(0)-1; i++)
            {
                length += i;
            }
            _savingTable = new SavingTableElement[length];
            var index = 0;

            for (int i = 1; i < data.GetLength(0); i++)
            {
                for (int j = i+1; j < data.GetLength(1); j++)
                {
                    _savingTable[index++] = new SavingTableElement
                    {
                        From = locations[i],
                        To = locations[j],
                        Distance = data[0, j] + data[0, i] - data[i, j]
                    };
                }
            }
        }

        public GoogleMapLocation[] GetWay()
        {
            var saving = _savingTable.ToList();
            var length = saving.Count;
            for (int i = 0; i < length; i++)
            {
                saving.Add(new SavingTableElement
                {
                    From = saving[i].To,
                    To = saving[i].From,
                    Distance = saving[i].Distance
                });
            }
            saving = saving.OrderByDescending(element => element.Distance).ToList();
            var to = saving[0].To;
            var way = new GoogleMapLocation[_wayLength];
            way[0] = saving[0].From;
            for (var i = 1; i < _wayLength-1; i++)
            {
                saving.RemoveAll(element => element.From == way[i - 1] || element.To == way[i - 1]);
                var el = saving.First(element => element.From == to);
                to = el.To;
                way[i]=el.From;
            }
            way[_wayLength - 1] = to;
            return way;
        }
        private class SavingTableElement
        {
            public GoogleMapLocation From { get; set; }
            public GoogleMapLocation To { get; set; }
            public float Distance { get; set; }
        }
    }
}
