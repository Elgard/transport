﻿using Transport.GoogleAPI;

namespace Transport.Code.TransportTask
{
    public class ClientAgent:Client
    {
        public bool IsDone { get; set; }
        public Direction Direction { get; set; }
        public int FindDistanceFromCenter()
        {
            Direction = new Direction(Location.Address, Center.Instance.Address);
            return DistanceFromCenter = Direction.Routes[0].Legs[0].Distance.Value;
        }

        public ClientAgent(Client client) : base(client)
        {
            IsDone = false;
        }
        public ClientAgent()
        {

        }

        public override string ToString()
        {
            return base.ToString()+$" {IsDone}";
        }
    }
}
