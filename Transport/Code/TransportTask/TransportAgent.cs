﻿using System;
using System.Collections.Generic;
using System.Linq;
using Transport.MapLoad;

namespace Transport.Code.TransportTask
{
    public class TransportAgent : Transport
    {

        public TransportAgent() { }

        public TransportAgent(List<WayPoint> way, Transport transport, GoogleMapLocation[] steps)
        {
            //Transport
            Name = transport.Name;
            Speed = transport.Speed;
            Capacity = transport.Capacity;
            MonetaryLossPerMeter = transport.MonetaryLossPerMeter;

            //TransportAgent
            _mapLocations = steps;
            Location = steps.First();
            _startCapacity = way.Sum(point => point.ClientAgent?.Mass??0);
            CurrentCapacity = _startCapacity;
            Distance = way.Sum(point => point.Distance);
            LocalTime = Distance / Speed;
            if (_globaltime < LocalTime)
            {
                _globaltime = LocalTime;
            }
            TimePoints = new Dictionary<float, WayPoint>();
            MoneyLoose = MonetaryLossPerMeter * Distance;
            foreach (var wayPoint in way)
            {
                TimePoints.Add(wayPoint.Distance / Speed, wayPoint);
            }
            
            SetTime(0);
        }
        #region Property block
        public float CurrentCapacity { get; set; }

        public float LocalTime { get; private set; }

        public Dictionary<float, WayPoint> TimePoints { get; set; }

        public float MoneyLoose { get; set; }

        public GoogleMapLocation TargetWayPoint { get; set; } = new GoogleMapLocation();
        public GoogleMapLocation Location { get; set; }
        public float Persent { get; private set; }

        public float Money
            =>
                Center.Instance.ClientAgents.Where(
                    agent => TimePoints.Values.Select(point => point.Location).Contains(agent.Location))
                    .Select(agent => agent.Price)
                    .Sum();

        public float Distance { get; private set; }
        #endregion

        private static float _globaltime;
        private GoogleMapLocation[] _mapLocations;
        private float _time, _startCapacity;

        public void SetTime(float time)
        {
            time *= _globaltime;
            _time = time;
            Persent = 100 / LocalTime * time;
            if (Persent > 100) Persent = 100;

            var dist = time*Speed;
            var sum = 0f;
            int i;
            for (i = 0; i < _mapLocations.Length-1 && sum < dist; i++)
            {
                sum += _mapLocations[i].Distance(_mapLocations[i + 1]);
            }
            Location = _mapLocations[i];
            var nextPoint = TimePoints.FirstOrDefault(pair => pair.Key >= time).Value;

            if (nextPoint == null || nextPoint.Location.Address != TargetWayPoint.Address)
            {

                var local = nextPoint?.Location ?? new GoogleMapLocation
                {
                    Address = Center.Instance.Address
                };
                TargetWayPoint = local;
                CurrentCapacity = _startCapacity;
                foreach (var value in TimePoints.Values.Where(point => point.ClientAgent!=null))
                {
                    value.ClientAgent.IsDone = false;
                }
                foreach (var point in TimePoints.Where(pair => pair.Key <= time && pair.Value.ClientAgent != null))
                {
                    if (point.Value.ClientAgent.IsTransportOnly)
                    {
                        CurrentCapacity += point.Value.ClientAgent.Mass;
                    }
                    else
                    {
                        CurrentCapacity -= point.Value.ClientAgent.Mass;
                    }
                    point.Value.ClientAgent.IsDone = true;
                }

            }

        }

        public void EditWay(List<WayPoint> way, GoogleMapLocation[] steps)
        {
            var location = new List<GoogleMapLocation>();
            foreach (var mapLocation in _mapLocations)
            {
                location.Add(mapLocation);
                if(mapLocation == Location)break;
            }
            location.AddRange(steps);
            _mapLocations = location.ToArray();
            Distance = 0;
            for (int i = 0; i < _mapLocations.Length-1; i++)
            {
                Distance += _mapLocations[i].Distance(_mapLocations[i + 1]);
            }
            LocalTime = Distance / Speed;
            if (_globaltime < LocalTime)
            {
                _globaltime = LocalTime;
            }
            var timePoints = new Dictionary<float, WayPoint>();
            foreach (var el in TimePoints.Where(pair => pair.Key<_time))
            {
                timePoints[el.Key] = el.Value;
            }
            MoneyLoose = MonetaryLossPerMeter * Distance;

            foreach (var wayPoint in way)
            {
                timePoints.Add(_time + wayPoint.Distance / Speed, wayPoint);
            }
            TimePoints = timePoints;
        }

        public override string ToString()
        {
            return $"{Name} Target Adress: {TargetWayPoint.Address}\n" +
                   $" CurrentCapacity: {CurrentCapacity} Positon [Lat: {Location.Latitude} Long: {Location.Longitude}] Compleat: {Persent}%";
        }


    }
}
