﻿using System;

namespace Transport.Code
{
    [Serializable]
    public class Transport
    {
        public string Name { get; set; }
        public float Speed { get; set; }
        public float Capacity { get; set; }
        public float MonetaryLossPerMeter { get; set; }
        public override string ToString()
        {
            return $"Name: {Name} Speed: {Speed} Capacity: {Capacity} Monetary: {MonetaryLossPerMeter}";
        }
    }
}
