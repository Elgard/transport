﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transport.Code.TransportTask;
using Transport.MapLoad;

namespace Transport.Code
{
    public class WayPoint
    {
        public ClientAgent ClientAgent { get; set; }
        public GoogleMapLocation Location { get; set; }
        public float Distance { get; set; }
    }
}
